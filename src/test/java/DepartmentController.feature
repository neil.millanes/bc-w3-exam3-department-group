Feature: Department Controller

  Scenario: Add a Department
    Given Create a Department object
    When Department object was created
    Then It will return save

  Scenario: Find a Department
    Given  A Department Id was given
    When Department id does exist
    Then It will return the department data
    When Department id does not exist
    Then It will return department not found

  Scenario: Delete a Department
    Given A Department Id was given
    When Department id does exist
    Then It will return delete
    When Department id does not exist
    Then It will return department not found

  Scenario: Update a Department
    Given A Department Id was given
    And Department object was created
    When Department id does exist
    Then It will update the department
    When Department id does not exist
    Then It will return department not found