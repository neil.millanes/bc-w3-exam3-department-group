package com.novare.bootcamp.week3.exam.group2.department.service.impl;

import com.novare.bootcamp.week3.exam.group2.department.dao.DepartmentDao;
import com.novare.bootcamp.week3.exam.group2.department.dao.EmployeeDao;
import com.novare.bootcamp.week3.exam.group2.department.model.Department;
import com.novare.bootcamp.week3.exam.group2.department.model.Employee;
import com.novare.bootcamp.week3.exam.group2.department.service.DepartmentDaoService;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import javassist.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class DepartmentDaoServiceImplTest {


    @InjectMocks
    DepartmentDaoServiceImpl departmentDaoService;

    @Mock
    EmployeeDaoServiceImpl employeeDaoService;

    @Mock
    DepartmentDao departmentDao;

    @Mock
    EmployeeDao employeeDao;

    List<Department> departmentList;
    Department department;
    Employee employee;

    List<String[]> csvList =new ArrayList<String[]>();
    List<Department> departments;
    @BeforeEach
    public void init() throws NotFoundException {
        employee = new Employee(1, "Employee", "Master");

        department = new Department("ITDepartment", employee, "BGC", "Novare");
        departmentList = List.of(
                new Department(1,"HR", employee, "BGC", "Novare"),
                new Department(2,"Maintenance", employee, "BGC", "Novare"),
                new Department(3,"Finance", employee,"BGC", "Novare")
        );
        String[] strings1 = {"HR", "1", "BGC", "Novare"};
        String[] strings2 = {"Maintenance", "1", "BGC", "Novare"};
        String[] strings3 = {"Finance", "1", "BGC", "Novare"};
        csvList.add(strings1);
        csvList.add(strings2);
        csvList.add(strings3);
    }

    @Test
    @DisplayName("This should return nothing when adding Department")
    void addDepartment() throws NotFoundException {
        departmentDaoService.addDepartment(department.getName(),
                department.getHead_employee().getId(),
                department.getLocation(), department.getCompany());
    }

    @Test
    @DisplayName("This should return all Department")
    void getAllDepartment() {
       Mockito.when(departmentDao.findAll()).thenReturn(departmentList);
       assertEquals(departmentList, departmentDaoService.getAllDepartment());
    }

    @Test
    @DisplayName("This should return the Department when getting it")
    void getDepartment() throws NotFoundException{
            departmentDaoService.addDepartment(department.getName(), department.getHead_employee().getId(),
                    department.getLocation(), department.getCompany());
            Mockito.when(departmentDao.existsById(department.getId())).thenReturn(true);
            Mockito.when(departmentDao.getOne(department.getId())).thenReturn(department);
            assertEquals(department, departmentDaoService.getDepartmentById(department.getId()));

    }
    @Test
    @DisplayName("This should throws NotFoundException when getting an Invalid Department")
    void getDepartmentException(){
        try {
            assertEquals(department, departmentDaoService.getDepartmentById(10));
        }catch (NotFoundException e){
            assertThrows(NotFoundException.class, () -> departmentDaoService.getDepartmentById(department.getId()));
        }
    }

    @Test
    @DisplayName("This should return nothing when updating Department")
    void updateDepartment()throws NotFoundException{
      departmentDaoService.addDepartment(department.getName(), department.getHead_employee().getId(),
              department.getLocation(), department.getCompany());
      Mockito.when(departmentDao.existsById(department.getId())).thenReturn(true);
      Mockito.when(departmentDao.save(department)).thenReturn(department);
      departmentDaoService.updateDepartment(department);
    }

    @Test
    @DisplayName("This should throws NotFoundException when updating an invalid Department")
    void updateDepartmentException(){
        try {
            departmentDaoService.updateDepartment(department);
        }catch (NotFoundException e){
            assertThrows(NotFoundException.class, () -> departmentDaoService.getDepartmentById(department.getId()));
        }

    }

    @Test
    @DisplayName("This should return nothing when deleting Department")
    void deleteDepartment()throws NotFoundException {
       departmentDao.save(department);
      Mockito.when(departmentDao.existsById(department.getId())).thenReturn(true);
      departmentDaoService.deleteDepartment(department.getId());

    }

    @Test
    @DisplayName("This should throws NotFoundException when deleting an invalid Department")
    void deleteDepartmentException(){
        try {
            departmentDaoService.deleteDepartment(department.getId());
        }catch (NotFoundException e){
            assertThrows(NotFoundException.class, () -> departmentDaoService.deleteDepartment(department.getId()));
        }

    }


    @Test
    void addDepartmentObject() {
        departmentDaoService.addDepartmentObject(department);
    }

    @Test
    void addDepartmentCsv()  {
        departmentDaoService.addDepartmentCsv("test.csv");
    }
    @Test
    void addDepartmentCsvException()  {
        try {
            CSVReader reader = new CSVReader(new FileReader("test.csv"));
        } catch (Exception e) {
            assertThrows(Exception.class, () ->
            departmentDaoService.addDepartmentCsv("Invalid.file"));
        }
    }


}