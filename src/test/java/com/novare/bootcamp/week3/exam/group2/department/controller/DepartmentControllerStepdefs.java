package com.novare.bootcamp.week3.exam.group2.department.controller;

import com.novare.bootcamp.week3.exam.group2.department.dao.DepartmentDao;
import com.novare.bootcamp.week3.exam.group2.department.model.Department;
import com.novare.bootcamp.week3.exam.group2.department.model.Employee;
import com.novare.bootcamp.week3.exam.group2.department.service.DepartmentDaoService;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import javassist.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.List;

public class DepartmentControllerStepdefs {


    Employee employee = new Employee(1 , "Employee", "Master");

    Department department = new Department(1, "name", employee, "location",
            "company");
    DepartmentController departmentController = new DepartmentController();

    @InjectMocks
    DepartmentDaoService departmentDaoService;

    @Mock
    DepartmentDao departmentDao;

    DepartmentControllerTest test;

    List<Department> departmentList;

    @BeforeEach
    public void init() throws NotFoundException {
        test = new DepartmentControllerTest();
        employee = new Employee(1, "Employee", "Master");

        department = new Department(4,"ITDepartment", employee, "BGC", "Novare");
        departmentList = List.of(
                new Department(1,"HR", employee, "BGC", "Novare"),
                new Department(2,"Maintenance", employee, "BGC", "Novare"),
                new Department(3,"Finance", employee,"BGC", "Novare")
        );
    }

    @Given("Create a Department object")
    public void createADepartmentObject() {
        department = new Department();
    }

    @When("Department object was created")
    public void departmentObjectWasCreated() {

    }

    @Then("It will return save")
    public void itWillReturnSave() throws Exception {
        test.addDepartmentTest("HR", 1, "BGC", "Novare");
    }

    @Given("A Department Id was given")
    public void aDepartmentIdWasGiven() {
       // Assertions.assertEquals(true, (department.getId() != 0));
    }

    @When("Department id does exist")
    public void departmentIdDoesExist() throws NotFoundException {
        // Assertions.assert
    }

    @Then("It will return the department data")
    public void itWillReturnTheDepartmentData() throws Exception {
      //  Assertions.assertEquals(true, departmentController.getDepartmentById(department.getId()));
    }

    @Then("It will return delete")
    public void itWillReturnDelete() throws Exception {
        test.deleteDepartmentTest(1);
    }

    @Then("It will update the department")
    public void itWillUpdateTheDepartment() throws Exception {
       test.updateDepartmentTest(department);
    }

    @When("Department id does not exist")
    public void departmentIdDoesNotExist() {
       // Assertions.assertEquals(false, department.getId());
    }

    @Then("It will return department not found")
    public void itWillReturnDepartmentNotFound() {
    }
}
