package com.novare.bootcamp.week3.exam.group2.department.controller;

import com.novare.bootcamp.week3.exam.group2.department.dao.DepartmentDao;
import com.novare.bootcamp.week3.exam.group2.department.model.Department;
import com.novare.bootcamp.week3.exam.group2.department.model.Employee;
import com.novare.bootcamp.week3.exam.group2.department.service.DepartmentDaoService;
import javassist.NotFoundException;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class DepartmentControllerTest {

    @Mock
    DepartmentDao departmentDao;

    @Autowired
    private DepartmentDaoService departmentDaoService;

    List<Department> departmentList;
    DepartmentController controller;
    Department department;
    Employee employee;

    @BeforeEach
    public void init() {
        controller = new DepartmentController();
        employee = new Employee(1, "Employee", "Master");

        department = new Department("ITDepartment", employee, "BGC", "Novare");
        departmentList = List.of(
                new Department(1,"HR", employee, "BGC", "Novare"),
                new Department(2,"Maintenance", employee, "BGC", "Novare"),
                new Department(3,"Finance", employee,"BGC", "Novare")
        );
    }

    @Test
    public void addDepartmentTest(String name, int id, String location, String company) throws Exception {
        Mockito.when(controller.addDepartment(name,id,location,company)).thenReturn(true);
        Assert.assertEquals(true, controller.addDepartment(name,id,location,company));
    }

    @Test
    public void getDepartmentTest() throws Exception {
        Mockito.when(controller.getDepartmentById(departmentList.get(1).getId())).thenReturn(department);
        Assert.assertEquals(department, controller.getDepartmentById(departmentList.get(1).getId()));
    }

    @Test
    public void updateDepartmentTest(Department department) throws Exception {
        Mockito.when(controller.updateDepartment(department)).thenReturn(true);
        Assert.assertEquals(true, controller.updateDepartment(department));
    }

    @Test
    public void deleteDepartmentTest(int id) throws Exception {
        Mockito.when(controller.deleteDepartmentById(id)).thenReturn(true);
        Assert.assertEquals(true, controller.deleteDepartmentById(id));
    }


}
