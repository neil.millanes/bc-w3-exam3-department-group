package com.novare.bootcamp.week3.exam.group2.department.service.impl;


import com.novare.bootcamp.week3.exam.group2.department.dao.DepartmentGroupDao;
import com.novare.bootcamp.week3.exam.group2.department.model.DepartmentGroup;
import com.novare.bootcamp.week3.exam.group2.department.service.DepartmentGroupDaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartmentGroupDaoServiceImpl implements DepartmentGroupDaoService {

    @Autowired
    DepartmentGroupDao departmentGroupDao;

    @Override
    public DepartmentGroup findDepartmentGroupById(int id) throws Exception {
        DepartmentGroup departmentGroup;
        try{
            departmentGroup = departmentGroupDao.findById(id).get();
        }catch (NullPointerException npe){
            throw new NullPointerException();
        }
        return departmentGroup;
    }
}
