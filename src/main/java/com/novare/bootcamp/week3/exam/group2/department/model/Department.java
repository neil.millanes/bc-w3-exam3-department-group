package com.novare.bootcamp.week3.exam.group2.department.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@JsonIgnoreProperties("head_employee")
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String location;
    private String company;
    private Date date_created;
    private Date date_updated;

    @ManyToOne
    private Employee head_employee;

    @OneToMany(mappedBy = "department")
    private Set<DepartmentGroup> departmentGroup_id;

    public Department() {
    }

    public Department(int id, String name, Employee head_employee, String location, String company) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.company = company;
        this.head_employee = head_employee;
    }

    public Department(String name, Employee head_employee, String location, String company) {
        this.name = name;
        this.head_employee = head_employee;
        this.location = location;
        this.company = company;
        this.date_created = new Date();
        this.date_updated = new Date();
    }

    public Department(String name, String location, String company, Date date_created, Date date_updated, Employee head_employee) {
        this.name = name;
        this.location = location;
        this.company = company;
        this.date_created = date_created;
        this.date_updated = date_updated;
        this.head_employee = head_employee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Date getDate_created() {
        return date_created;
    }

    public void setDate_created(Date date_created) {
        this.date_created = date_created;
    }

    public Date getDate_updated() {
        return date_updated;
    }

    public void setDate_updated(Date date_updated) {
        this.date_updated = date_updated;
    }

    public Set<DepartmentGroup> getDepartmentGroup_id() {
        return departmentGroup_id;
    }

    public void setDepartmentGroup_id(Set<DepartmentGroup> departmentGroup_id) {
        this.departmentGroup_id = departmentGroup_id;
    }

    public Employee getHead_employee() {
        return head_employee;
    }

    public void setHead_employee(Employee head_employee) {
        this.head_employee = head_employee;
    }
}
