package com.novare.bootcamp.week3.exam.group2.department.controller;


import com.novare.bootcamp.week3.exam.group2.department.model.Department;
import com.novare.bootcamp.week3.exam.group2.department.service.DepartmentDaoService;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DepartmentController {

    @Autowired
    private DepartmentDaoService departmentDaoService;

    //Add New Department
    @PostMapping(value="/add/department/{name}/{headId}/{location}/{company}")
    public boolean addDepartment(@PathVariable("name") String name,
                             @PathVariable("headId") int headId,
                             @PathVariable("location") String location,
                             @PathVariable("company") String company) throws Exception {
        departmentDaoService.addDepartment(name, headId, location, company);
        return true;
    }

    @PostMapping(value="/add/department")
    public void addDepartmentParam(@RequestParam("name") String name,
                                   @RequestParam("headId") int headId,
                                   @RequestParam("location") String location,
                                   @RequestParam("company") String company) throws Exception {
        departmentDaoService.addDepartment(name, headId, location, company);
    }

    //Add New Department by Object
    @PostMapping(value="/addDepartment/")
    public void addDepartmentObject(@RequestBody Department department) throws Exception {
        departmentDaoService.addDepartmentObject(department);
    }

    //Add New Department By CSV
    @PostMapping(value="add/department/csv/{file_path}")
    public void addDepartmentCsv(@PathVariable String file_path){
        departmentDaoService.addDepartmentCsv(file_path);
    }

    //Retrieve All Departments
    @GetMapping(value="/get/departments", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Department> getAllDepartment() {
        return departmentDaoService.getAllDepartment();
    }

    //Retrieve Department By Id
    @GetMapping(value="/get/department/{department_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Department getDepartmentById(@PathVariable int department_id) throws Exception {
        return departmentDaoService.getDepartmentById(department_id);
    }

    @PutMapping(value="/update/department/")
    public boolean updateDepartment(@RequestBody Department department) throws Exception{
        departmentDaoService.updateDepartment(department);
        return true;
    }

    @DeleteMapping(value="/delete/department/{department_id}")
    public boolean deleteDepartmentById(@PathVariable int department_id) throws  Exception{
        departmentDaoService.deleteDepartment(department_id);
        return true;
    }

    @DeleteMapping(value="/delete/department")
    public void deleteDepartmentParam(@RequestParam("id") int department_id) throws  Exception{
        departmentDaoService.deleteDepartment(department_id);
    }
}
