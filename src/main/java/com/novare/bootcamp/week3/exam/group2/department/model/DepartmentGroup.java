package com.novare.bootcamp.week3.exam.group2.department.model;

import javax.persistence.*;

@Entity
public class DepartmentGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    @ManyToOne
    private Department department;

    public DepartmentGroup() {
    }

    public DepartmentGroup(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
