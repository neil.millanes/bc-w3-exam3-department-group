package com.novare.bootcamp.week3.exam.group2.department.service.impl;


import com.novare.bootcamp.week3.exam.group2.department.dao.DepartmentDao;
import com.novare.bootcamp.week3.exam.group2.department.model.Department;
import com.novare.bootcamp.week3.exam.group2.department.service.DepartmentDaoService;
import com.novare.bootcamp.week3.exam.group2.department.service.EmployeeDaoService;
import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import javassist.NotFoundException;
import org.aspectj.weaver.ast.Not;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;


@Service
public class DepartmentDaoServiceImpl implements DepartmentDaoService {

    @Autowired
    DepartmentDao departmentDao;

    @Autowired
    EmployeeDaoService employeeDaoService;

    @Override
    public void addDepartment(String name, int head_employee, String location, String company)
            throws NotFoundException {
        Date date = new Date();
        Department department = new Department(name, location, company, date, date,
                employeeDaoService.findEmployeeById(head_employee));
        departmentDao.save(department);
    }

    @Override
    public Department addDepartmentObject(Department department) {
        return departmentDao.save(department);
    }

    @Override
    public void addDepartmentCsv(String file_path) {
        try (CSVReader reader = new CSVReader(new FileReader(file_path))) {
            List<String[]> list_array = reader.readAll();
            list_array.remove(0);
            List<Department> departments = new ArrayList<>();
            list_array.forEach(r -> {
                try {
                    departments.add(
                            new Department(r[0],employeeDaoService.findEmployeeById(Integer.parseInt(r[1])),r[2],r[3]));
                } catch (NotFoundException e) {
                    e.printStackTrace();
                }
            });
            departments.forEach(department -> departmentDao.save(department));
        } catch (IOException | CsvException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Department> getAllDepartment() {
        return departmentDao.findAll();
    }

    @Override
    public Department getDepartmentById(int id) throws NotFoundException {
        boolean isExisting = departmentDao.existsById(id);
        if (isExisting) {
            return (departmentDao.getOne(id));
        }else{
            throw new NotFoundException("Department does not exist!");
        }
    }

    @Override
    public Department updateDepartment(Department department) throws NotFoundException {
        boolean isExisting = departmentDao.existsById(department.getId());
        if (isExisting) {
            return departmentDao.save(department);
        } else {
            throw new NotFoundException("Department does not exist!");
        }
    }

    @Override
    public void deleteDepartment(int id) throws NotFoundException {
        boolean isExisting = departmentDao.existsById(id);
        if(isExisting){
            departmentDao.deleteById(id);

        }else {
            throw new NotFoundException("Department does not exist!");
        }
    }
}
