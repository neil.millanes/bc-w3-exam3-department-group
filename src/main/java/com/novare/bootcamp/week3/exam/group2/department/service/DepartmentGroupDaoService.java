package com.novare.bootcamp.week3.exam.group2.department.service;


import com.novare.bootcamp.week3.exam.group2.department.model.DepartmentGroup;

public interface DepartmentGroupDaoService {

    DepartmentGroup findDepartmentGroupById(int id) throws Exception;
}