package com.novare.bootcamp.week3.exam.group2.department.service;


import com.novare.bootcamp.week3.exam.group2.department.model.Department;
import com.novare.bootcamp.week3.exam.group2.department.model.Employee;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface DepartmentDaoService {

    void addDepartment(String name, int head_employee, String location, String company) throws NotFoundException;

    Department addDepartmentObject(Department department);

    void addDepartmentCsv(String file_path);

    List<Department> getAllDepartment();

    Department getDepartmentById(int id) throws  NotFoundException;

    Department updateDepartment(Department department) throws NotFoundException;

    void deleteDepartment(int id) throws NotFoundException;
}
