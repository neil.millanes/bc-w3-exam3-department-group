package com.novare.bootcamp.week3.exam.group2.department.dao;

import com.novare.bootcamp.week3.exam.group2.department.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeDao extends JpaRepository<Employee, Integer> {
}