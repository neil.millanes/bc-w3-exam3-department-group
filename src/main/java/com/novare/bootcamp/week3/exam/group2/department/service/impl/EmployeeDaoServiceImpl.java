package com.novare.bootcamp.week3.exam.group2.department.service.impl;


import com.novare.bootcamp.week3.exam.group2.department.dao.EmployeeDao;
import com.novare.bootcamp.week3.exam.group2.department.model.Employee;
import com.novare.bootcamp.week3.exam.group2.department.service.EmployeeDaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeDaoServiceImpl implements EmployeeDaoService {

    @Autowired
    EmployeeDao employeeDao;

    @Override
    public Employee findEmployeeById(int id) {
        Employee employee;
        try{
            employee = employeeDao.findById(id).get();
        }catch (NullPointerException npe){
            throw new NullPointerException();
        }
        return employee;
    }
}
