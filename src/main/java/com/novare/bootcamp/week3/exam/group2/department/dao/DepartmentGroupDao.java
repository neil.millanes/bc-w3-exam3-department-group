package com.novare.bootcamp.week3.exam.group2.department.dao;

import com.novare.bootcamp.week3.exam.group2.department.model.DepartmentGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentGroupDao extends JpaRepository<DepartmentGroup, Integer> {
}

