package com.novare.bootcamp.week3.exam.group2.department.service;

import com.novare.bootcamp.week3.exam.group2.department.model.Employee;
import javassist.NotFoundException;

public interface EmployeeDaoService {

    Employee findEmployeeById(int id) throws NotFoundException;
}
