package com.novare.bootcamp.week3.exam.group2.department.dao;


import com.novare.bootcamp.week3.exam.group2.department.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentDao extends JpaRepository<Department, Integer> {
}
